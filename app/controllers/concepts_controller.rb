class ConceptsController < ApplicationController
  # GET /concepts
  # GET /concepts.json
  def index
    
    @concepts = Concept.search_title(params[:search])
    
    if (@concepts).size == 1
      redirect_to edit_concept_path( @concepts.first)
    else
      @concepts = params[:search].present? ? Concept.search_title_and_body(params[:search]) : Concept.completes 
      @concepts = Concept.all if @concepts.size == 0
      respond_to do |format|
        format.html # index.html.erb
        format.json { render json: @concepts }
      end
    end
  end

  # GET /concepts/1
  # GET /concepts/1.json
  def show
    @concept = Concept.find(params[:id])
    redirect_to edit_concept_path(@concept)
    
    # 1.times {@concept.leads_to_links.build}
    # 1.times {@concept.caused_by_links.build}
    # @concept.taggings.build
    # 
    # @differential_tags = @concept.childrens_tags.push(Tag.new).push(Tag.new).push(Tag.new)
    # (@concept.childrens_tags - @concept.asks).each {|t| @concept.askables.build(:tag_id=>t.id)}
    # 
    # 
    # respond_to do |format|
    #   format.html # show.html.erb
    #   format.json { render json: @concept.self_json }
    # end
  end

  # GET /concepts/new
  # GET /concepts/new.json
  def new
    @concept = Concept.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @concept }
    end
  end

  # GET /concepts/1/edit
  def edit
    @concept = Concept.find(params[:id])
    1.times {@concept.treated_withs.build}
    1.times {@concept.caused_bys.build}
    1.times {@concept.leads_tos.build}
    @concept.complications.build
    @concept.findings.build
    @concept.children.build
  end

  # POST /concepts
  # POST /concepts.json
  def create
    @concept = Concept.new(params[:concept])

    respond_to do |format|
      if @concept.save
        format.html { redirect_to @concept, notice: 'Concept was successfully created.' }
        format.json { render json: @concept, status: :created, location: @concept }
      else
        format.html { render action: "new" }
        format.json { render json: @concept.errors, status: :unprocessable_entity}
      end
    end
  end

  # PUT /concepts/1
  # PUT /concepts/1.json
  def update
    # raise params.to_yaml
    # a = params[:concept][:leads_tos_attributes]
    # 
    # a = a.collect do |k,v|
    #   if v[:name].present? and v[:id].nil?
    #     c = Concept.reference_or_create_by_name(v[:name].strip)
    #     [k,{id: c.id}]
    #   else
    #     nil
    #   end
    # end
    # 
    # params[:concept][:leads_tos_attributes] = a
    # 
    # raise Hash[a].to_yaml
    

    @concept = Concept.find(params[:id])

    respond_to do |format|
      if @concept.update_attributes(params[:concept])
        format.html { redirect_to @concept, notice: 'Concept was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @concept.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /concepts/1
  # DELETE /concepts/1.json
  def destroy
    @concept = Concept.find(params[:id])
    @concept.destroy

    respond_to do |format|
      format.html { redirect_to concepts_url }
      format.json { head :no_content }
    end
  end
end
