class Tagging < ActiveRecord::Base
  attr_accessible :concept_id, :rules_out_if, :tag_id, :tag_name
  
  belongs_to :concept
  belongs_to :tag
  
  def tag_name
    self.tag.name if self.tag
  end
  
  def tag_name=(name)
    if name.present?
      self.tag = Tag.reference_or_create_by_name(name)
    else
      self.tag = nil
    end
  end
  
end
