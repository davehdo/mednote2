class Concept < ActiveRecord::Base
  attr_accessible :long_description, :name, :observable, :short_description, :testable, :culpable, :actionable, :triage_rule, :taggings_attributes, :caused_bys_attributes, :treated_withs_attributes, :symptoms_attributes, :findings_attributes, :askables_attributes, :leads_tos_attributes, :subtypes_attributes, :complications_attributes, :children_attributes, :parent_id
  
  validates :short_description, :length=>{:maximum=>255}
  validates :name, :length=>{:maximum=>255}
  
  has_many :leads_to_links, :class_name => "Causation", :foreign_key=>:concept_a_id, :dependent=>:destroy
      has_many :leads_tos, :through => :leads_to_links, :source=>:leads_to, :select=>"concepts.*,causations.id as causation_id"
        accepts_nested_attributes_for :leads_tos, :reject_if=> proc { |attributes| attributes['name'].blank? }, :allow_destroy=>true
      has_many :treated_withs, :through => :leads_to_links, :source=>:leads_to, :conditions=>"concepts.actionable = 't'", :after_add=>:mark_as_actionable, :select=>"concepts.*,causations.id as causation_id" 
        accepts_nested_attributes_for :treated_withs, :reject_if=> proc { |attributes| attributes['name'].blank? }, :allow_destroy=>true

      has_many :complications, :through => :leads_to_links, :source=>:leads_to, :conditions=>{:culpable=>true}, :after_add=>:mark_as_culpable, :select=>"concepts.*,causations.id as causation_id"
          accepts_nested_attributes_for :complications, :reject_if=> proc { |attributes| attributes['name'].blank? }, :allow_destroy=>true

    
      has_many :findings, :through => :leads_to_links, :source=>:leads_to, :conditions=>"concepts.testable = 't' or concepts.observable = 't'", :after_add=>:mark_as_finding, :select=>"concepts.*,causations.id as causation_id"
        accepts_nested_attributes_for :findings, :reject_if=> proc { |attributes| attributes['name'].blank? }, :allow_destroy=>true

        has_many :signs, :through => :leads_to_links, :source=>:leads_to, :conditions=>"concepts.testable = 't'", :after_add=>:mark_as_finding, :select=>"concepts.*,causations.id as causation_id"
          accepts_nested_attributes_for :findings, :reject_if=> proc { |attributes| attributes['name'].blank? }, :allow_destroy=>true
  
  
  has_many :caused_by_links, :class_name => "Causation", :foreign_key=>:concept_b_id, :dependent=>:destroy
    has_many :caused_bys, :through=>:caused_by_links, :source=>:leads_from, :after_add=> :mark_as_culpable   
      accepts_nested_attributes_for :caused_bys, :reject_if=> proc { |attributes| attributes['name'].blank? }, :allow_destroy=>true
  
  # has_many :subtypes, :dependent=>:destroy
  #   accepts_nested_attributes_for :subtypes, :reject_if => proc { |attributes| attributes['name'].blank? }
  
  has_many :children, :class_name=>"Concept", :foreign_key=>"parent_id" , :after_add=>:recalc_n_children
  accepts_nested_attributes_for :children , :reject_if => :blank_name_and_id , :allow_destroy=>true
  belongs_to :parent, :class_name=>"Concept"
  
  before_destroy :reject_if_has_children
  
  
  def caused_bys_two_degrees
    (caused_bys + caused_bys.collect {|e| e.caused_bys}).flatten
  end
  
  def blank_name_and_id( attributes ) 
    attributes['name'].blank? and attributes['id'].blank?
  end
  
  
  def children_and_parent #can be a scope
    children + [parent].compact
  end
  
  def recalc_n_children( r=nil )
    if r and r.new_record?
      self.n_children = children.size
    else
      update_attribute :n_children, children.size
    end
    true
  end
  
  def reject_if_has_children
    children.size==0
  end
  
  def long_description_pretty
    if self.long_description.present?
      self.long_description.gsub(/^(i+)\.\s?(.+)\s?$/i) {"<h#{$1.size+1}>#{$2}</h#{$1.size+1}>"}
      .gsub(/^([^<>]+?)$/, '<p>\1</p>')
      # .gsub(/^\ *(.*)\ *\=\>\ *(.*)\ *$/) {"<p>#{pre($1)} => #{pre($2)}</p>"}
      # .gsub(/^\ *(.*)\ *\-\>\ *(.*)\ *$/) {"<p>#{pre($1)} -> #{pre($2)}</p>"}
      # .gsub(/\=\>/) {"<i class=\"icon-arrow-right\"></i>"}
      # .gsub(/\-\>/) {"<i class=\"icon-share-alt\"></i>"}
      # .gsub(/\{([^\{\}]+)\}/) {"<pre>#{$1}</pre>"}
      # .gsub(/\[(.+?)\]/) {|e| t=Topic.link_to($1); t ? t : e  }
    else
      ""
    end
  end
  
  # scopes and stuff
  def self.orphans # can be used as a scope
    a = joins(:caused_by_links).count("causations.id", :group=>"concepts.id")  
    b = joins(:leads_to_links).count("causations.id", :group=>"concepts.id")  
    where("id NOT IN (#{a.keys.concat(b.keys).join(",")})")
  end
  
  scope :culpables, where("concepts.culpable = 't'")
  scope :findings, where("concepts.testable = 't' or concepts.observable = 't'")
  scope :treatments, where("concepts.actionable = 't'")
  
  scope :blanks, where(:long_description=>nil, :short_description=>nil)
  scope :untouched, where("updated_at = created_at")
  scope :present, where("long_description IS NOT NULL or short_description IS NOT NULL")
  scope :contentful, where("long_description IS NOT NULL") # used in the right-hand side listing of concepts
  scope :alphabetical_listing, order("lower(name) ASC").select("id,name,long_description,triage_rule")
  
  def earliest_descendent
    if parent
      parent.earliest_descendent
    else
      self
    end
  end
  
  def mark_as_actionable( concept )
    if concept.new_record?
      concept.culpable = false
      concept.observable = false
      concept.testable = false
    end
    concept.actionable = true
    true
  end
  
  def mark_as_culpable( concept )
    if concept.new_record?
      concept.observable = false
      concept.testable = false
      concept.actionable = false
    end
    concept.culpable = true
    true
  end
  
  def mark_as_finding( concept )
    if concept.new_record?
      concept.culpable = false
      concept.actionable = false
    end
    concept.observable = true
    concept.testable = true
    
    true
  end
  
  
  # attr_accessor :referenced_subtypes
  # 
  # def referenced_subtypes # when concept is obtained as an association to another...
  #   if self.attributes.keys.include? "causation_id"
  #     Causation.find(self.causation_id).subtypes
  #   else
  #     @referenced_subtypes || []
  #   end
  # end
    
  def before_save_collection_association # used as a before_save callback to check while saving a collection association 
    # for each of the new leads_tos (the ones without ids), reference or create by name
    try_to_reference([ :complications, :findings, :leads_tos, :treated_withs, :caused_bys])
    try_to_reference_children
    true
  end

  def try_to_reference_children
    [:children].each do |assoc|
      new_array= self.send(assoc).collect do |l|
        if  l.marked_for_destruction?
          nil
        elsif l.new_record?
          new_concept = Concept.reference_or_create_by_name(l.name)
          if (new_concept.id == self.id) 
            nil # eliminate self-references
          elsif !new_concept.parent_id.nil?

            nil # eliminate references to concepts that already have a parent
          else
            new_concept
          end
        else
          l
        end
      end.compact.uniq 
      
      self.n_children = new_array.size 
        
      self.send("#{assoc}=", new_array)
    end
  end
  
  def try_to_reference( associations_array )
    associations_array.each do |assoc|
      new_array= self.send(assoc).collect do |l|
        if l.new_record?
          new_concept = Concept.reference_or_create_by_name(l.name)
          if (new_concept.id == self.id) 
            nil # eliminate self-references
          else
            new_concept
          end
        else
          l
        end
      end.compact.uniq       
      self.send("#{assoc}=", new_array)
    end
  end
  

  
  def self.typeahead_array
    "[" + select("name").collect{|e|"\"#{e.name}\""}.join(',')  + "]"
  end
  
  def display_name
    (self.name || "Unnamed concept") + (self.worriable ? " <i class=\"icon-warning-sign\"></i>" : "")
  end
  
  def worriable
    (self.triage_rule || 0) >= 30
  end
  
  # methods assessing the completeness of the concept
  def is_blank? 
    self.short_description.nil? and self.long_description.nil?
  end
  
  def untouched?
    (self.created_at == self.updated_at)
  end
  
  def orphaned?
    leads_to_links.size == 0 and caused_by_links.size == 0
  end
  
  def partial?
    self.long_description.nil? or self.long_description.size<300
  end
  
  #  
  def destroy_if_blank
    self.destroy if self.is_blank? and self.orphaned? and self.untouched?
  end
  
  def specific_signs( omit_concept=[], nItems = 7, nest = false )
    omit_concept = [omit_concept] if omit_concept.class != Array
    signs = (self.leads_tos.where("observable = 't' OR testable = 't'" + (nest ? " OR culpable = 't'" : "")) - omit_concept).collect{|e|(e.name || "") + ((e.culpable? and nest) ? " #{e.specific_signs(omit_concept + [e],2,false)}" : "")}
    if signs.size == 0
      nil
    elsif signs.size < nItems
      "(#{signs.join(", ")})"
    else
      "(#{signs.first(nItems).join(", ")}, ...)"
    end
  end


  def self.search_title(search_str=nil)
    if search_str.present?
      where("lower(name) = '#{search_str.downcase}'")
    else
      []
    end
  end

  
  def self.search_title_and_body(search_str=nil)
    if search_str.present?
      search_str.downcase!
      top_results = where("lower(name) LIKE '%#{search_str}%' or lower(short_description) LIKE '%#{search_str}%'")
      bottom_results = where("lower(long_description) LIKE '%#{search_str}%'")
      top_results.concat( bottom_results - top_results )
    else
      all
    end
  end
   
  def traits
    ["observable", "testable", "culpable", "actionable"].select {|e| self.send(e)}
  end
  
  
  def self.partials
    all.select {|e|e.partial?}
  end

  def self.completes
    where("long_description IS NOT NULL").select {|e|e.long_description and e.long_description.size>=300}
  end

  
  
  def self.triage_rules
    [
      ["Defer to causes and complications", nil],
      ["Unspecified", 0],
      ["Dr", 10],
      ["UC", 20],
      ["ED", 30]
    ]
  end
  
  def childrens_tags 
    (self.caused_bys + self.leads_tos).collect {|e|e.tags}.flatten.uniq
  end
  
  def self_json
    concepts = self.caused_bys + self.leads_tos
    
    {
      name: self.name, 
      differential: concepts.collect{|e|e.name}, 
      children: self.triage_rule.nil? ? Concept.branch_using_tag(concepts, self.asks) : [],
      triage: self.triage_rule.nil? ? Concept.highest_triage_in_words(concepts) : "Auto #{Concept.triage_rules.rassoc(self.triage_rule)}"
    }
  end
  
  def ruled_out_by?(tag, response)
    # response is true/false, corresponding to yes/no response to the tag prompt
    tagging = self.taggings.where(tag_id: tag.id).first
    if tagging.nil?
      false
    else
      tagging.rules_out_if == response
    end
  end
  
  def self.highest_triage_for(concepts)
    if concepts.size == 0
      nil
    else
      triages = concepts.collect{|e|e.triage_rule}.compact
      if triages.size == 0
        nil
      else
        triages.sort{|a,b|a<=>b}.last
      end
    end
  end
  
  def self.highest_triage_in_words( concepts )
    if concepts.size == 0
      "Defer"
    elsif concepts.size == 1
      "To #{Concept.triage_rules.rassoc( concepts.first.triage_rule)}"
    else
      "Up to #{Concept.triage_rules.rassoc( Concept.highest_triage_for(concepts))}"
    end
  end
  
  def self.branch_using_tag(suspected_concepts, tags)
    if tags.size > 0 and suspected_concepts.size > 1
      
      # go through tags until find oen that has nonzero discerability for the current differential
      tag = tags.shift
      while !tag.nil? and tag.discernability(suspected_concepts) == 0 #(tags.size > 0 ) and (!defined?(tag) or  ) do
        tag = tags.shift
      end
      
      # if there was a tag that worked...
      if !tag.nil?
        concepts_if_yes = suspected_concepts.select{|e|!e.ruled_out_by?(tag, true) }
        concepts_if_no = suspected_concepts.select{|e|!e.ruled_out_by?(tag, false) }
        [
          {
            name: "#{tag.name} = #{tag.yes_text}",
            differential: concepts_if_yes.collect{|e|e.name},
            children: Concept.branch_using_tag(concepts_if_yes, tags.clone),
            triage: Concept.highest_triage_in_words(concepts_if_yes)
          },
          {
            name: "#{tag.name} = #{tag.no_text}", 
            differential: concepts_if_no.collect{|e|e.name}, 
            children: Concept.branch_using_tag(concepts_if_no, tags.clone),
            triage: Concept.highest_triage_in_words(concepts_if_no)
          }
        ]
      else
        []
      end
    else
      []
    end
  end
  
  def self.fuzzy_search(name)
    where("lower(name) = ?", name.downcase)
  end
  
  def self.case_inv_search(name)
    where("lower(name) = ?", name.downcase)
  end
  
  def self.reference_or_create_by_name( name )
    if name.blank?
      nil
    else
      name.strip!
      a = Concept.case_inv_search(name)
      if a.size > 0
        a[0]
      else
        create(:name=>name)
      end
    end
  end

  
# e.g. name_string is cough:wet:frequent where cough is the concept and wet and frequent are subtypes
  # def self.reference_or_create_by_name_and_subtype( name_string=nil )
  #   if name_string.blank? or (name_parts = name_string.split(":")).first.blank?
  #     nil
  #   else
  #     # find or create 
  #     name = name_parts.shift
  #     concept = Concept.reference_or_create_by_name(name)      
  #     
  #     concept.referenced_subtypes = concept.create_subtypes( name_parts )
  #     concept
  #   end
  # end

  
  # def create_subtypes( subtype_name_array=[] )
  #   existing_subtypes = self.subtypes.collect{|e|e.name.downcase}
  #   subtype_name_array.collect {|e|e.strip}.select {|e|!existing_subtypes.include? e.downcase}.uniq{|e|e.downcase}.collect do |name|
  #     self.subtypes.create(:name=>name)
  #   end
  # end
end
