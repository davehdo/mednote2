class Tag < ActiveRecord::Base
  attr_accessible :name, :no_text, :prompt, :yes_text
  
  has_many :taggings


  has_many :askables
  has_many :askers, :through=>:askables, :source=>:concept
  
  def self.options
    select("id,name").order("name ASC").collect{|e|[e.name,e.id]}
  end
  
  def self.reference_or_create_by_name( name )
    if name.blank?
      nil
    else
      a = where("lower(name) = ?", name.downcase)
      if a.size > 0
        a[0]
      else
        create(:name=>name)
      end
    end
  end
  
  # gives the tag a score to represent how useful it is for narrowing a given differential
  # higher is better
  def discernability( concepts )
    concept_ids = concepts.collect{|e|e.id}
    taggings = self.taggings.where(concept_id: concept_ids)
    original_size = concepts.size * 1.0
    
    size_a = original_size - taggings.select{|e|e.rules_out_if==true}.size
    size_b = original_size - taggings.select{|e|e.rules_out_if==false}.size
    
    score_a = size_a==0 ? 1.0 : size_a / original_size
    score_b = size_b==0 ? 1.0 : size_b / original_size
    
    1.0 - (score_a ** 2+score_b ** 2)/2.0
  end
  
end
