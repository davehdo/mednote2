class Causation < ActiveRecord::Base
  attr_accessible :concept_a_id, :concept_b_id, :leads_to_name, :leads_from_name
  
  belongs_to :leads_to, :class_name=>"Concept", :foreign_key=>:concept_b_id
  belongs_to :leads_from, :class_name=>"Concept", :foreign_key=>:concept_a_id
  
  has_many :typables
  has_many :subtypes, :through=>:typables
  
  after_destroy :destroy_related_concepts_if_blank
  
  # after_create :add_subtypes
  # 
  # def add_subtypes
  #   self.subtypes = leads_to.referenced_subtypes
  # end
  
  def destroy_related_concepts_if_blank
    self.leads_to.reload.destroy_if_blank
    self.leads_from.reload.destroy_if_blank
  end
  
end
