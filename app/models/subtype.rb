class Subtype < ActiveRecord::Base
  belongs_to :concept
  has_many :typables
  attr_accessible :name
  
end
