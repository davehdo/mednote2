class Askable < ActiveRecord::Base
  attr_accessible :concept_id, :ask_order, :tag_id
  belongs_to :concept
  belongs_to :tag
end
