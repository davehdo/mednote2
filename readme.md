## Intro
A lightweight medical note-taking app that builds a medical ontology while you use it. I built this and used it during medical school when trying to organize the vast differentials and arrays of supporting evidence for different diagnoses.

## How it works
This web based app has an interface similar to Evernote. Users can create a page to take notes on a particular concept, like 'chest pain'. Within the text users can denote relationships with other medical terms, using the below notation, which gets extracted from the text and placed into the sidebar. As you build pages and connections, linkages develop, allowing you to navigate back and forth between concepts. On the 'chest pain' page, as an emergent property, you can see the different causes, and recommendations on which examinations or tests to run to differentiate them. 

Chest pain differential

* myocardial infarction (Troponin Elevated )
* pulmonary embolism (Troponin Elevated )
* pneumothorax 
* pericarditis (Troponin Elevated , diffuse st elevation, pulsus paradoxus)
* aortic dissection 
* peptic ulcer 
* Pneumonia (Cough, hypoxemia, tachypnea, wheezing, fever, oxygen sat < 95, fatigue, ...)
* esophageal perforation  (bloody sputum, severe pain)


To denote causal relationships, or the natural history of a disease
```
STEMI, pericarditis -> chest pain
chest pain -> troponin elevated
pericarditis -> pulsus paradoxus
```

or if on the chest pain page, the shorthand version is 

```
STEMI ->
```

To denote a typical or recommended intervention
```
chest pain => EKG
```