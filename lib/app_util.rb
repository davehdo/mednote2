module MyActiveRecordExtensions

  extend ActiveSupport::Concern

  # add your instance methods here
  def foo
     "foo"
  end

  # add your static(class) methods here
  module ClassMethods
    def bar
      "bar"
    end
    

    
  end
end

# include the extension 
ActiveRecord::Base.send(:include, MyActiveRecordExtensions)
