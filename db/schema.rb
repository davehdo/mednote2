# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20130217033323) do

  create_table "askables", :force => true do |t|
    t.integer   "concept_id"
    t.integer   "tag_id"
    t.integer   "ask_order"
    t.timestamp "created_at", :null => false
    t.timestamp "updated_at", :null => false
  end

  create_table "causations", :force => true do |t|
    t.integer   "concept_a_id"
    t.integer   "concept_b_id"
    t.boolean   "triggerable",  :default => true
    t.timestamp "created_at",                     :null => false
    t.timestamp "updated_at",                     :null => false
  end

  create_table "concepts", :force => true do |t|
    t.string    "name"
    t.string    "short_description"
    t.text      "long_description"
    t.boolean   "observable",        :default => true
    t.boolean   "testable",          :default => false
    t.boolean   "culpable",          :default => true
    t.integer   "triage_rule"
    t.timestamp "created_at",                           :null => false
    t.timestamp "updated_at",                           :null => false
    t.boolean   "actionable",        :default => false
    t.integer   "parent_id"
    t.integer   "n_children",        :default => 0
  end

  create_table "subtypes", :force => true do |t|
    t.integer   "concept_id"
    t.string    "name"
    t.timestamp "created_at", :null => false
    t.timestamp "updated_at", :null => false
  end

  add_index "subtypes", ["concept_id"], :name => "index_subtypes_on_concept_id"

  create_table "taggings", :force => true do |t|
    t.integer   "tag_id"
    t.integer   "concept_id"
    t.boolean   "rules_out_if", :default => false
    t.integer   "order"
    t.timestamp "created_at",                      :null => false
    t.timestamp "updated_at",                      :null => false
  end

  create_table "tags", :force => true do |t|
    t.string    "name"
    t.text      "prompt"
    t.string    "yes_text",   :default => "Yes"
    t.string    "no_text",    :default => "No"
    t.timestamp "created_at",                    :null => false
    t.timestamp "updated_at",                    :null => false
  end

  create_table "typables", :force => true do |t|
    t.integer   "subtype_id"
    t.integer   "causation_id"
    t.boolean   "leads_to"
    t.timestamp "created_at",   :null => false
    t.timestamp "updated_at",   :null => false
  end

  add_index "typables", ["causation_id"], :name => "index_typables_on_causation_id"
  add_index "typables", ["subtype_id"], :name => "index_typables_on_subtype_id"

end
