class AddActionableToConcept < ActiveRecord::Migration
  def change
    add_column :concepts, :actionable, :boolean, :default=>false
  end
end
