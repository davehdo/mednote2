class CreateConcepts < ActiveRecord::Migration
  def change
    create_table :concepts do |t|
      t.string :name
      t.string :short_description
      t.text :long_description
      t.boolean :observable, :default=>true
      t.boolean :testable, :default=>false
      t.boolean :culpable, :default=>true
      t.integer :triage_rule
      
      t.timestamps
    end
  end
end
