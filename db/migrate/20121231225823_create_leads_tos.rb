class CreateLeadsTos < ActiveRecord::Migration
  def change
    create_table :causations do |t|
      t.integer :concept_a_id
      t.integer :concept_b_id
      t.boolean :triggerable, :default=>true
      t.timestamps
    end
  end
end
