class CreateSubtypes < ActiveRecord::Migration
  def change
    create_table :subtypes do |t|
      t.references :concept
      t.string :name

      t.timestamps
    end
    add_index :subtypes, :concept_id
  end
end
