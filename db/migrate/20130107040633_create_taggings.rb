class CreateTaggings < ActiveRecord::Migration
  def change
    create_table :taggings do |t|
      t.integer :tag_id
      t.integer :concept_id
      t.boolean :rules_out_if, :default=>false
      t.integer :order
      t.timestamps
    end
  end
end
