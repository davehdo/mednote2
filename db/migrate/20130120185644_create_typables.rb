class CreateTypables < ActiveRecord::Migration
  def change
    create_table :typables do |t|
      t.references :subtype
      t.references :causation
      t.boolean :leads_to

      t.timestamps
    end
    add_index :typables, :subtype_id
    add_index :typables, :causation_id
  end
end
