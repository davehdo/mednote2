class AddParentToConcept < ActiveRecord::Migration
  def change
    add_column :concepts, :parent_id, :integer
    add_column :concepts, :n_children, :integer, :default=>0
  end
end
