class CreateTags < ActiveRecord::Migration
  def change
    create_table :tags do |t|
      t.string :name
      t.text :prompt
      t.string :yes_text, :default=>"Yes"
      t.string :no_text, :default=>"No"

      t.timestamps
    end
  end
end
