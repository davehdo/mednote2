class CreateAskables < ActiveRecord::Migration
  def change
    create_table :askables do |t|
      t.integer :concept_id
      t.integer :tag_id
      t.integer :ask_order

      t.timestamps
    end
  end
end
