require 'spec_helper'

describe "Concepts" do
  describe "GET /concepts" do
    it "works" do
      get concepts_path
      response.status.should be(200)
    end
  end
  
  describe "GET /concepts/new" do
    it "works" do
      get new_concept_path
      response.status.should be(200)
    end
  end
  
  
end
