require 'spec_helper'

describe Concept do
  describe "destroying children" do
    before :each do 
      @concept = Concept.create!(name: "Parent")
      @child = @concept.children.create!(name: "child")  
      @concept.update_attributes(children_attributes: {"1"=> {id: @child.id, _destroy: true }} )
      
    end
    
    it "should not delete the term" do
      Concept.find_by_name("child").should_not == nil
    end
    
    it "should remove from children list" do
      @concept.reload.children.should == []
    end
    
    it "should update n_children" do
      @concept.reload.n_children.should == 0
    end
    
  end
  
  describe "creating children" do
    before :each do 
      @concept = Concept.create!(name: "Parent")
      @child = @concept.children.create!(name: "child")  
      @concept.n_children.should == 1
    end
      
    it "should update n_children" do
      @concept.n_children.should == 1  
    end
    
    it "should update child's parent_id attribute" do
      @child.parent_id.should == @concept.id
    end
  end
  
  describe "referencing exisitng children" do
    before :each do 
      @concept = Concept.create!(name: "Parent")
      @child = @concept.children.create!(name: "child")  
      @concept.n_children.should == 1
    end
    
    it "should work" do
      @existing = Concept.create(name: "existing")
      @concept.update_attributes(children_attributes: {"1"=> {name: @existing.name}})
      @concept.reload.children.should include(@existing)
      @existing.reload.parent.should == @concept
    end

    it "should update n_children" do
      @existing = Concept.create(name: "existing")
      @concept.update_attributes(children_attributes: {"1"=> {name: "existing"}})
      @concept.children.size.should == 2
      @concept.n_children.should == 2
    end
            
    it "should not allow stealing children from another" do
      parent2 = Concept.create(name: "Parent2", children_attributes: {"1"=> {name: "child"}})
      @child.reload.parent == @concept
    end
    
    it "should have accurate n_children for both parents after attempted steal" do
      parent2 = Concept.create(name: "Parent2", children_attributes: {"1"=> {name: "child"}})
      parent2.reload.n_children.should == 0
      @concept.reload.n_children.should == 1
    end
    
  end
  
  # describe "caused_by" do
  #   it "should include childrens caused_by as own" do
  #     @concept = Concept.create(name: "Parent")
  #     child = @concept.children.create(name: "child")
  #     causer = child.caused_bys.create(name: "causer")
  #     child.caused_bys.should == [causer]
  #     @concept.caused_bys.should == [causer]
  #   end    
  # end
  
  describe "specific_signs method" do
    it "should include findings" do
      @concept = Concept.create(name: "original")
      @concept.findings.create(name: "finding")
      @concept.specific_signs.include?("finding").should == true
    end

    it "should include complications" do
      @concept = Concept.create(name: "original")
      @comp  = @concept.complications.create(name: "comp")
      # @comp.culpable.should == true
      # @comp.observable.should == false
      # @comp.testable.should == false
      @concept.specific_signs.include?("comp").should == true
    end

    
    it "should include findings of complications" do
      @concept = Concept.create(name: "original")
      @comp = @concept.complications.create(name: "comp")
      @comp.findings.create(name: "compfinding")
      @concept.specific_signs.include?("compfinding").should == true      
    end
  end
  
  describe "on destroy" do
    it "should not destroy if has children" do
      concept = Concept.create(name: "Parent")
      concept.children.create(name:"Child")
      concept.destroy
      concept.destroyed?.should==false
    end
    
    it "should delete findings associations" do
      concept = Concept.create(:name=>"cold", :long_description=>"hi")
      concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache"}})
      concept.leads_to_links.size.should == 1
      (c=Concept.find_by_name("headache")).destroy
      concept.reload.leads_to_links.size.should == 0
      
    end

    it "should delete caused_bys associations" do
      concept = Concept.create(:name=>"cold", :long_description=>"hi")
      concept.update_attributes(:caused_bys_attributes=>{"1"=> {name: "headache"}})
      concept.caused_by_links.size.should == 1
      (c=Concept.find_by_name("headache")).destroy

      concept.reload.caused_by_links.size.should == 0
      
    end
    
    
  end
  
  describe "saving a concept as a finding" do
    it "should not be allowed because still a duplicate" do
      concept = Concept.create(:name=>"flu")
      concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache"}})
      concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache"}})
      Causation.all.size.should==1
    end
    
    
    it "should create and label as observable" do
      concept = Concept.create(:name=>"flu")
      concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache"}})
      Concept.find_by_name("headache").observable.should == true
    end
    
    it "should label as observable but not change any other labels if already exists" do
      headache = Concept.create(:name=>"headache", :observable=>false, :culpable=>true)
      concept = Concept.create(:name=>"flu")
      concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache"}})
      headache = headache.reload
      headache.observable.should == true
      headache.culpable.should == true
    end
    
    # it "should reference existing but not reclassify as observable" do
    #   headache = Concept.create(name: "headache", observable: false)
    #   concept = Concept.create(:name=>"flu")
    #   concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache"}})
    #   headache.reload.observable.should == false
    # end
  end
  
  # describe "saving with leads_to name = 'headache:diffuse'" do
  #   it "should create a concept headache and link to it via causation" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "headache:diffuse"}})
  #     (c=Concept.find_by_name("headache")).should_not == nil
  #     concept.leads_tos.first.should == c
  #     
  #   end
  #   
  #   it "should create a subtype of headache called diffuse" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cough:dry"}})
  #     Concept.find_by_name("cough").subtypes.where(:name=>"dry").size.should == 1
  #     # c.referenced_subtypes.should == [Subtype.find_by_name("dry")]
  #   end
  # 
  #   it "should allow access of the subtype by referenced_subtypes method" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cough:dry"}})
  #     concept.reload.leads_tos.first.referenced_subtypes.should == [Subtype.find_by_name("dry")]    
  #   end
  #     
  #   it "should trim names for subtypes" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cough: dry "}})
  #     (c=concept.leads_tos.first).name.should == "cough"
  #     concept.reload.leads_tos.first.referenced_subtypes.should == [Subtype.find_by_name("dry")]
  #   end
  # end
  # 
  # describe "saving with symptoms name = 'headache:diffuse'" do
  #   it "should create a concept headache and link to it via causation" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:findings_attributes=>{"1"=> {name: "headache:diffuse"}})
  #     (c=Concept.find_by_name("headache")).should_not == nil
  #     concept.findings.first.should == c
  #   end
  #   
  #   it "should create a subtype of headache called diffuse" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:findings_attributes=>{"1"=> {name: "cough:dry"}})
  #     Concept.find_by_name("cough").subtypes.where(:name=>"dry").size.should == 1
  #     # c.referenced_subtypes.should == [Subtype.find_by_name("dry")]
  #   end
  # 
  #   it "should allow access of the subtype by referenced_subtypes method" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:findings_attributes=>{"1"=> {name: "cough:dry"}})
  #     concept.reload.findings.first.referenced_subtypes.should == [Subtype.find_by_name("dry")]    
  #   end
  #     
  #   it "should trim names for subtypes" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.update_attributes(:findings_attributes=>{"1"=> {name: "cough: dry "}})
  #     (c=concept.findings.first).name.should == "cough"
  #     concept.reload.findings.first.referenced_subtypes.should == [Subtype.find_by_name("dry")]
  #   end
  # end
  
  # describe "referenced_subtypes method" do
  #   it "should fail nicely if none" do
  #     concept = Concept.create(:name=>"cold")
  #     concept.referenced_subtypes.should == []
  #   end
  # end
     
  describe "saving with nested treated_withs" do
    it "should create a new concept and mark it as actionable" do
      concept = Concept.create(:name=>"flu")
      concept.update_attributes(:treated_withs_attributes=>{"1"=> {name: "tamiflu"}})
      (t=Concept.find_by_name("tamiflu")).should_not == nil
      t.actionable.should == true
    end
  end
  
  describe "saving with nested leads_tos" do
    it "should create a new concept by name" do
      concept = Concept.create(:name=>"cold")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cough"}})
      Concept.find_by_name("cough").should_not == nil
    end

    it "should reference 2 new concepts by name" do
      concept = Concept.create(:name=>"cold")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cough"},"2"=> {name: "cough2"}})
      concept.leads_tos.size.should == 2
    end

    it "should reference a concept by name" do
      concept = Concept.create(:name=>"cold")
      concept2 = Concept.create(:name=>"cough")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cough"}})
      Concept.find_by_name("cough").should == concept2
    end
    
    it "should not allow referencing self" do
      concept = Concept.create(:name=>"cold")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "cold"}})
      concept.leads_tos.size.should == 0
    end
    
    it "should ignore blank name" do
      concept = Concept.create(:name=>"cold")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: ""}})
      concept.leads_tos.size.should == 0
    end
    
    it "should reference using a trimmed name" do
      concept = Concept.create(:name=>"cold")
      concept2 = Concept.create(:name=>"cough")
      
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: " cough "}})
      Concept.find_by_name("cough").should == concept2
    end
    
    it "should reference with case insensitivity" do
      concept = Concept.create(:name=>"cold")
      concept2 = Concept.create(:name=>"cough")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "Cough"}})
      Concept.find_by_name("cough").should == concept2
    end
    
    it "should not allow duplicate" do
      concept = Concept.create(:name=>"cold")
      concept.update_attributes(:leads_tos_attributes=>{"1"=> {name: "headache"}, "2"=> {name: "headache"}})
      concept.reload.leads_tos.size.should == 1
    end
    
    
  end
  
  describe "destroying a leads_to" do
    it "should destroy linkage " do
      concept = Concept.create(:name=>"cold")
      concept2 = concept.leads_tos.create(:name=>"headache", :long_description=>"1")
      concept.leads_tos.size.should == 1
      concept.update_attributes(:leads_tos_attributes=>{"1"=>{"_destroy"=>"true", "id"=>concept2.id}})
      concept.leads_tos.size.should == 0
    end
    
    it "should NOT destroy the concept if descriptions present" do
      concept = Concept.create(:name=>"cold")
      concept2 = concept.leads_tos.create(:name=>"headache", :long_description=>"1")
      concept.update_attributes(:leads_tos_attributes=>{"1"=>{"_destroy"=>"true", "id"=>concept2.id}})
      Concept.find_by_name("headache").should_not == nil
    end
    
    it "should destroy referenced concept if no descriptions or causations" do
      concept = Concept.create(:name=>"cold")
      concept2 = concept.leads_tos.create(:name=>"headache")
      concept.leads_tos.size.should == 1
      concept.update_attributes(:leads_tos_attributes=>{"1"=>{"_destroy"=>"true", "id"=>concept2.id}})
      concept.leads_tos.size.should == 0
      Concept.find_by_name("headache").should == nil
    end

    it "should NOT destroy referenced topic if has other causations" do
      concept = Concept.create(:name=>"cold")
      concept2 = concept.leads_tos.create(:name=>"headache")

      concept3 = Concept.create(:name=>"migraine")
      concept4 = concept3.leads_tos.create(:name=>"headache")

      concept.leads_tos.size.should == 1
      concept.update_attributes(:leads_tos_attributes=>{"1"=>{"_destroy"=>"true", "id"=>concept2.id}})
      concept.leads_tos.size.should == 0
      Concept.find_by_name("headache").should_not == nil
    end
    
  end
  
  
  # describe "create_subtypes method" do
  #   it "should create subtype" do
  #     concept = Concept.create(:name=>"headache")
  #     concept.create_subtypes(["diffuse", "focal"])
  #     concept.subtypes.size.should == 2
  #     concept.subtypes.first.name.should == "diffuse"
  #     concept.subtypes.last.name.should == "focal"
  #   end
  #   
  #   it "should not duplicate subtype if same name" do
  #     concept = Concept.create(:name=>"headache")
  #     concept.create_subtypes(["diffuse", "diffuse"])
  #     concept.subtypes.size.should == 1
  #     concept.subtypes.first.name.should == "diffuse"
  #   end
  #   
  # end
  
  
  describe "upon saving" do
      # it "should creat causations to all the other concepts that were referenced" do
      #   @referenced_concept = Concept.create(:name=>"Neat")
      #   @concept = Concept.create!(:name=>"Also neat", :long_description=>"i.Heading\nSee [#{@referenced_Concept.id}]")
      #   @Concept.referenced_concepts.should == [@referenced_concept]
      # end
    
      # it "should create a pointer with the name of the heading stored in the join table" do
      #   @referenced_concept = Concept.create(:name=>"Neat")
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Things\ni.Attributes\nSee [#{@referenced_Concept.id}]")
      #   (p = @Concept.pointers).size.should == 1
      #   p.first.heading.should == "Attributes"
      # end
      #     
      # it "should erase pointer when reference removed from text" do
      #   @referenced_concept = Concept.create(:name=>"Neat")
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nSee [#{@referenced_Concept.id}]")
      #   @Concept.referenced_concepts.should == [@referenced_concept]
      # 
      #   @Concept.update_attribute(:long_description, "")
      #   @Concept.reload.referenced_concepts.should == []
      # end
      #     
      # it "should create two pointers" do
      #   @referenced_concept1 = Concept.create(:name=>"Neat")
      #   @referenced_concept2 = Concept.create(:name=>"Kewl")
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nSee [#{@referenced_concept1.id}] or [#{@referenced_concept2.id}] ")
      #   @Concept.referenced_concepts.should == [@referenced_concept1,@referenced_concept2]
      # end
      # 
      # it "should not create if reference id is invalid" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nSee [123]")
      #   @Concept.pointers.should == []
      #   @Concept.referenced_concepts.should == []      
      # end

      # it "should create new concepts based on references" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones->Jones\nthe end")
      #   (b=Concept.find_by_name("Bones")).should_not == nil
      #   (j=Concept.find_by_name("Jones")).should_not == nil
      # end
      #     
      # it "should create a causation with right id references for each causal relationship defined" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones->Jones\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      # 
      #   (b=Concept.find_by_name("Bones"))
      #   (j=Concept.find_by_name("Jones"))
      # 
      #   r.causer_id.should==b.id
      #   r.causee_id.should == j.id      
      # end
      #     
      # it "should store concepts and subconcepts when concept:subconcept" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones:big->Jones:small\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      # 
      #   (b=Concept.find_by_name("Bones")).should_not == nil
      #   (j=Concept.find_by_name("Jones")).should_not == nil
      # 
      #   r.causer_id.should==b.id
      #   r.causer_subtype.should == "Big"
      #   r.causee_id.should == j.id
      #   r.causee_subtype.should == "Small"
      # end
      #     
      # it "should capitalize the first letter" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nbones:big->Jones:small\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      # 
      #   (b=Concept.find_by_name("Bones")).should_not == nil      
      #   r.causer_subtype.should == "Big"
      # end
      #     
      # it "should reference existing with case insensitivity" do
      #   @b = Concept.create(:name=>"BONES")
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nbones->Jones\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      #   r.causer_id.should == @b.id
      # end
      #     
      # it "should accept BLANK->asdf" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\n->Jones:\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      # 
      #   (j=Concept.find_by_name("Jones")).should_not == nil
      # 
      #   r.causer_id.should==@Concept.id
      #   r.causee_id.should == j.id
      # end
      #     
      # it "should accept asdf->BLANK" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones->\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      # 
      #   (b=Concept.find_by_name("Bones")).should_not == nil
      #   r.causer_id.should==b.id
      #   r.causee_id.should == @Concept.id
      # end
      #     
      # it "should accept asdf:->sdf:" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones:->Jones:\nthe end")
      #   (m=@Concept.causations).size.should == 1
      #   r=m.first
      # 
      #   (b=Concept.find_by_name("Bones")).should_not == nil
      #   (j=Concept.find_by_name("Jones")).should_not == nil
      # 
      #   r.causer_id.should==b.id
      #   r.causer_subtype.should == nil
      #   r.causee_id.should == j.id
      #   r.causee_subtype.should == nil
      # end
      #     
      # it "should not duplicate same causation" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones ->Jones\nthe end")
      #   @Concept.update_attributes(:name=>"Also neat", :long_description=>"i.Heading\nBones ->Jones\nthe end")
      #   (m=@Concept.causations).size.should == 1
      # end
      #     
      # it "should erase relation when removed from the text" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones ->Jones\nthe end")
      #   (m=@Concept.causations.first).should_not == nil
      #   @Concept.update_attributes( :long_description=>"i.Heading\nremoved\nthe end")
      #   m.destroyed?.should==true
      # end
      #     
      # it "should erase concepts without bodies if their references are erased too" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones ->Jones\nthe end")
      #   (b = Concept.find_by_name("Bones")).should_not == nil
      #   @Concept.update_attributes(:name=>"Also neat", :long_description=>"i.Heading\nBlah\nthe end")
      #   Concept.find_by_name("Bones").should == nil
      # 
      # end
      # 
      # it "should NOT erase concepts without bodies if still has references" do
      #   @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nBones ->Jones\nthe end")
      #   Concept.create!(:name=>"Also neat2", :long_description=>"i.Heading\nBones ->Moans\nthe end")
      #   (b = Concept.find_by_name("Bones")).should_not == nil
      #   @Concept.update_attributes(:name=>"Also neat", :long_description=>"i.Heading\nBlah\nthe end")
      #   Concept.find_by_name("Bones").should_not == nil
      # end
      #     
      #     
      # describe "when given array" do
      #   before :each do
      #     @concept = Concept.create(:name=>"Also neat", :long_description=>"i.Heading\nARF:ATN -> Urine:Full brownish pigment, Urine:granular casts with epithelial casts\nthe end")
      #   end
      #   it "should generate one relation for each" do
      #     (r=@Concept.causations).size.should == 2
      #     r.first.causee_id.should==r.last.causee_id 
      #   end
      # 
      #   it "should recognize if multiple elements have name:subtype format" do
      #     (b=Concept.where(:name=>"Urine")).size.should == 1
      #   end
      # end
  end
  
  # describe "find_or_create_by_name_i" do
  #   it "should not create anything with unmatched ()[] chars on the edge" do
  #     Concept.find_or_create_by_name_i("(This").name.should == "This"
  #     Concept.find_or_create_by_name_i("This)").name.should == "This"
  #     Concept.find_or_create_by_name_i("Or(This)").name.should == "Or(This)"
  # 
  #   end
  # 
  # end
  
 
  
end
