require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe ConceptsController do

  # This should return the minimal set of attributes required to create a valid
  # Concept. As you add validations to Concept, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    {}
  end

  # This should return the minimal set of values that should be in the session
  # in order to pass any filters (e.g. authentication) defined in
  # ConceptsController. Be sure to keep this updated too.
  def valid_session
    {}
  end
  # 
  # describe "nested taggings_attributes" do
  #   it "should save if tag_name and rules_out_if are defined" do
  #     @concept = Concept.create! valid_attributes
  #     @concept.update_attributes( {taggings_attributes: {1=> {tag_name: "yippie", rules_out_if: true}, 2=> {tag_name: "yippie2", rules_out_if: false}} })
  #     @concept.tags.size.should == 2
  #   end
  #   
  #   it "should save if tag_id and rules_out_if are defined" do
  #     @concept = Concept.create! valid_attributes
  #     @tag = Tag.create!(name: "wow")
  #     @concept.update_attributes( {taggings_attributes: {1=> {tag_id: @tag.id, rules_out_if: true} }})
  #     @concept.tags.size.should == 1
  #   end
  #   
  #   it "should not create tagging if rules_out_if is not defined" do
  #     @concept = Concept.create! valid_attributes
  #     @concept.update_attributes( {taggings_attributes: {1=> {tag_name: "yippie"}, 2=> {tag_name: "yippie2", rules_out_if: ""}} })
  #     @concept.tags.size.should == 0
  #     
  #   end
  #   
  #   it "should accept if id is defined and tag_name is not" do
  #     @concept = Concept.create! valid_attributes
  #     @tagging = @concept.taggings.create(tag_name: "abc", rules_out_if: false)
  #     @concept.tags.size.should == 1
  #     @concept.taggings.first.rules_out_if.should == false
  #     @concept.update_attributes( {taggings_attributes: {1=> {id: @tagging.id, rules_out_if: true}} })
  #     @concept.taggings.size.should == 1
  #     @concept.taggings.first.rules_out_if.should == true
  #   end
  #   
  #   it "should create if given attr {tag_name: 'young', rules_out_if: 'false'} where false is text" do
  #     @concept = Concept.create! valid_attributes
  #     @concept.update_attributes({taggings_attributes: {1=> {tag_name: 'young', rules_out_if: 'false'}}})
  #     @concept.tags.size.should == 1
  #   end
  #   
  #   it "should destroy if _destory is 1 and id is specified" do
  #     @concept = Concept.create! valid_attributes
  #     @tagging = @concept.taggings.create(tag_name: "abc", rules_out_if: false)
  #     @concept.tags.size.should == 1
  #     @concept.update_attributes(taggings_attributes: {1=> {id: @tagging.id, _destroy: "1" }})
  #     @concept.tags.size.should == 0
  #   end
  #   
  # end
  
  describe "GET index" do
  #   it "assigns all concepts as @concepts" do
  #     concept = Concept.create! valid_attributes
  #     get :index, {}, valid_session
  #     assigns(:concepts).should eq([concept])
  #   end
    
    describe "with search param" do
      it "should go directly to result if there is one" do
        concept = Concept.create(:name=>"Horseshoe")
        concept2 = Concept.create(:name=>"Horseshoe number two")
        get :index, search: "Horseshoe"
        response.should redirect_to edit_concept_path(concept)
      end
      
      it "should return everything if no matching results" do
        concept = Concept.create(:name=>"Horseshoe")
        concept = Concept.create(:name=>"Horseshoe number two")
        get :index, search: "something else"
        assigns(:concepts).should eq(Concept.all)
        
      end
      
      it "should find in name with case insensitivity" do
        concept = Concept.create(:name=>"Horseshoe")
        get :index, search: "horse"
        assigns(:concepts).should eq([concept])
      end
      
      it "should find in subject line" do
        concept = Concept.create(:name=>"unrelated", :short_description=>"Horseshoe")
        get :index, search: "horse"
        assigns(:concepts).should eq([concept])
        
      end
      
      it "should find in body" do
        concept = Concept.create(:name=>"unrelated", :long_description=>"Horseshoe")
        get :index, search: "horse"
        assigns(:concepts).should eq([concept])
        
      end
    end
  end
  # 
  # describe "GET show" do
  #   it "assigns the requested concept as @concept" do
  #     concept = Concept.create! valid_attributes
  #     get :show, {:id => concept.to_param}, valid_session
  #     assigns(:concept).should eq(concept)
  #   end
  # end
  # 
  # describe "GET new" do
  #   it "assigns a new concept as @concept" do
  #     get :new, {}, valid_session
  #     assigns(:concept).should be_a_new(Concept)
  #   end
  # end
  # 
  describe "GET edit" do
    it "assigns the requested concept as @concept" do
      concept = Concept.create! valid_attributes
      get :edit, {:id => concept.to_param}, valid_session
      assigns(:concept).should eq(concept)
      response.status.should == 200
    end
  end
  # 
  # describe "POST create" do
  #   describe "with valid params" do
  #     it "creates a new Concept" do
  #       expect {
  #         post :create, {:concept => valid_attributes}, valid_session
  #       }.to change(Concept, :count).by(1)
  #     end
  # 
  #     it "assigns a newly created concept as @concept" do
  #       post :create, {:concept => valid_attributes}, valid_session
  #       assigns(:concept).should be_a(Concept)
  #       assigns(:concept).should be_persisted
  #     end
  # 
  #     it "redirects to the created concept" do
  #       post :create, {:concept => valid_attributes}, valid_session
  #       response.should redirect_to(Concept.last)
  #     end
  #   end
  # 
  #   describe "with invalid params" do
  #     it "assigns a newly created but unsaved concept as @concept" do
  #       # Trigger the behavior that occurs when invalid params are submitted
  #       Concept.any_instance.stub(:save).and_return(false)
  #       post :create, {:concept => {}}, valid_session
  #       assigns(:concept).should be_a_new(Concept)
  #     end
  # 
  #     it "re-renders the 'new' template" do
  #       # Trigger the behavior that occurs when invalid params are submitted
  #       Concept.any_instance.stub(:save).and_return(false)
  #       post :create, {:concept => {}}, valid_session
  #       response.should render_template("new")
  #     end
  #   end
  # end
  # 
  # describe "PUT update" do
  #   describe "with valid params" do
  #     it "updates the requested concept" do
  #       concept = Concept.create! valid_attributes
  #       # Assuming there are no other concepts in the database, this
  #       # specifies that the Concept created on the previous line
  #       # receives the :update_attributes message with whatever params are
  #       # submitted in the request.
  #       Concept.any_instance.should_receive(:update_attributes).with({'these' => 'params'})
  #       put :update, {:id => concept.to_param, :concept => {'these' => 'params'}}, valid_session
  #     end
  # 
  #     it "assigns the requested concept as @concept" do
  #       concept = Concept.create! valid_attributes
  #       put :update, {:id => concept.to_param, :concept => valid_attributes}, valid_session
  #       assigns(:concept).should eq(concept)
  #     end
  # 
  #     it "redirects to the concept" do
  #       concept = Concept.create! valid_attributes
  #       put :update, {:id => concept.to_param, :concept => valid_attributes}, valid_session
  #       response.should redirect_to(concept)
  #     end
  #   end
  # 
  #   describe "with invalid params" do
  #     it "assigns the concept as @concept" do
  #       concept = Concept.create! valid_attributes
  #       # Trigger the behavior that occurs when invalid params are submitted
  #       Concept.any_instance.stub(:save).and_return(false)
  #       put :update, {:id => concept.to_param, :concept => {}}, valid_session
  #       assigns(:concept).should eq(concept)
  #     end
  # 
  #     it "re-renders the 'edit' template" do
  #       concept = Concept.create! valid_attributes
  #       # Trigger the behavior that occurs when invalid params are submitted
  #       Concept.any_instance.stub(:save).and_return(false)
  #       put :update, {:id => concept.to_param, :concept => {}}, valid_session
  #       response.should render_template("edit")
  #     end
  #   end
  # end
  # 
  # describe "DELETE destroy" do
  #   it "destroys the requested concept" do
  #     concept = Concept.create! valid_attributes
  #     expect {
  #       delete :destroy, {:id => concept.to_param}, valid_session
  #     }.to change(Concept, :count).by(-1)
  #   end
  # 
  #   it "redirects to the concepts list" do
  #     concept = Concept.create! valid_attributes
  #     delete :destroy, {:id => concept.to_param}, valid_session
  #     response.should redirect_to(concepts_url)
  #   end
  # end

end
